/*PSEUDO CODE

1. Program Utama : 

	a. DEKLARASI
		N, jumlah : integer
		
		procedure hitungJumlah (Input/Output a,j : integer)
		
	b. ALGORITMA
		read (N)
		hitungJumlah(N, jumlah)
		write (jumlah) 
	
2. Procedure :
	
	a. DEKLARASI
		i, jumlah : integer
		
	b. ALGORITMA
		jumlah <--0
		
		for i <-- 0 to a do
			write (i)
			jumlah <-- jumlah + i
		end for
		*J <-- jumlah
*/

#include <iostream>
using namespace std;

void hitungJumlah(int a, int *J);

int main()
{
	int N, jumlah;
	
	cout<<"\t---Program Jumlah N Buah Bilangan Genap---"<<endl;
	cout<<"========================================================="<<endl;
	cout<<"Masukan Nilai Terbesar Bilangan : ";cin>>N;
	hitungJumlah(N, &jumlah);
	cout<<"\n\nJumlah Bilangan = "<<jumlah;
}

void hitungJumlah(int a, int *J)
{
	int i, jumlah=0;
	
	for (i=0; i<=a; i+=2)
	{
		cout<<i<<"  ";
		jumlah = jumlah + i;
	}
	*J = jumlah;
}
