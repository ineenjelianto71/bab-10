/*PSEUDO CODE
1. Utama :
	a, b : Integer
	
	procedure (input, a,b : integer)
	
	read (a)
	read (b)
	kpk (a,b)
2. Procedure :
	x, nilai : integer
	
	nilai <-- 0
	
	for x <-- 1 to b do
		nilai = nilai + a
		if nilai % b = 0 then
			write (nilai)
		end if
	end for
*/
#include <iostream>
using namespace std;

void kpk(int a, int b);

int main()
{
	int a,b;
	
	cout<<"Masukkan Nilai Bil A : ";cin>>a;
	cout<<"Masukkan Nilai Bil B : ";cin>>b;
	kpk(a,b);
	
	return 0;
}

void kpk(int a, int b)
{
	int x, nilai;
	
	nilai = 0;
	
	for(x=1; x<=b; x++)
	{  
		nilai = nilai + a;
		if(nilai % b==0)
		{
			cout<<"KPK Antara "<<a<<" dan "<<b<<" Adalah "<<nilai;
			break;
		} 
	}
}
