/*PSEUDO CODE
1. Utama : 
	bil : integer
	procedure piramida (input a:integer)

	read (bil)
	piramida (bil)

2. Procedure :
	x,y,z : integer
	
*/
#include <iostream>
using namespace std;

void piramida(int a);

int main()
{
	int bil;
	
	cout<<"Masukan tinggi piramida : ";cin>>bil;
	piramida (bil);
}

void piramida(int a)
{
	int x, y, z;
	for( x = 1; x <= a; x++ )
	{
		for( z = a; z >= x; z-- )
			cout << " ";
		for( z = x; z < 2*x; z++ )
		{
			y = z % 10;
			cout << y;
		}
		for( z = 2*(x-1); z >= x; z-- )
		{
			y = z % 10;
			cout << y;
		}
	cout << endl;
	}
}
